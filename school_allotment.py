#!/bin/python3

school_seats_array = [1, 3, 1, 2]
student_score_array = [990, 780, 830, 860, 920]
student_school_pref_array = [   [3, 2, 1],
                                [3, 0, 0], 
                                [2, 0, 1], 
                                [0, 1, 2], 
                                [0, 2, 3] ]

"""
task: determine the TOTAL number of seats that are NOT filled and the TOTAL number of
students who have NOT been assigned a school
"""
def allocateSchools(school_seats_array, student_score_array, student_school_pref_array):
    """
    The following three lines of code creates an array of tuples with the structure (student_id, score).
    It is then sorted by the scores so the student at index 0 scored highest..
    """
    students = []
    students.extend( (stdnt_id, score) for stdnt_id, score in enumerate(student_score_array) )
    students.sort(key=lambda x: x[1], reverse=True)
    i = 0
    while i < len(students): # Iterate through the list of students
        student = students[i]
        student_id = student[0]
        for school in student_school_pref_array[student_id]: # Iterate through the students preferred schools
            if school_seats_array[school] > 0: # Check to see if the school has seats available
                school_seats_array[school] -= 1 # Decrement the seats available for that school 
                students.remove(student) # Remove the student from the array once they've been allocated a seat
                i -= 1 # Decrement the counter to keep iterating correctly (we removed an element from the list)
                break
        i += 1
    """
    The code returns the correct number of un-allocated seats however it does not return the correct number
    of un-allocated students. I'm not sure why as I simply did not have enough time to debug fully.
    """
    return [ sum(school_seats_array),  len(students)]

if __name__ == '__main__':
    print(allocateSchools(school_seats_array, student_score_array, student_school_pref_array))
