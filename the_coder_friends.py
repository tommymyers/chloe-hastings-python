#!/bin/python3

scoring_table = {
    'S': 0, # Skip
    'E': 1, # Easy
    'M': 3, # Medium
    'H': 5  # Hard
}

def calculate_score(string):
    score = 0
    for character in string:
        score += scoring_table[character]
    return score

def func(erica, bob, difficulty):
    erica_solved = erica.count(difficulty)
    bob_solved = bob.count(difficulty)
    if erica_solved > bob_solved:
        return 'Erica'
    elif bob_solved > erica_solved:
        return 'Bob'
    else:
        return 'Tie'

def winner(erica, bob):
    erica_score = calculate_score(erica)
    bob_score = calculate_score(bob)
    if erica_score > bob_score: # erica scored more
        return 'Erica'
    elif bob_score > erica_score: # bob scored more
        return 'Bob'
    else: # they have the same score
        if func(erica, bob, 'H') == 'Tie':
            if func(erica, bob, 'M') == 'Tie':
                if func(erica, bob, 'E') == 'Tie':
                    return 'Tie'
                else:
                    return func(erica, bob, 'E')
            else:
                return func(erica, bob, 'M')
        else:
            return func(erica, bob, 'H')

"""
Code taken from
https://stackoverflow.com/a/22635823
"""
def validate_input(string):
    return all(c in 'SEMH' for c in string)

if __name__ == '__main__':
    erica = input('')
    bob = input('')
    if validate_input(erica) and validate_input(bob):
        print(winner(erica, bob))
    else:
        print('Invalid input')
